package de.htw.util;

public class MagicFunction implements Function {
    public Map<Date, Double> compute(double[] x, double[] y);
}