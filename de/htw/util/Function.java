package de.htw.util;

import java.util.Date;
import java.util.Map;

/**
 * An interface for some math. stuff.
 *
 *
 * @author Erik Mautsch
 *
 */
public interface Function {

	public Map<Date, Double> compute(double[] x, double[] y);

}
