package de.htw.energyman;

import java.util.Random;
import java.util.Date;

/**
 * Gas subclass to Energy
 *
 * @author  Robert
 * @author Pedro
 * @author Paul
 * @version  1.0
 */
public class Gas extends Energy implements Comparable<Gas> {

    private Boolean friendly = Boolean.FALSE;

    public Gas() {
        super();
    }

    public Gas(String name) {
        super(name);
    }

    public Gas(String name, float performance, Date date) {
        super(name, performance, date);
    }

    public String compute() {
        return "This is test: " + new Random().nextDouble();
    }

    /**
     * @return  the friendly
     */
    public Boolean getFriendly() {
        return friendly;
    }

    /**
     * @param friendly the friendly to set
     */
    public void setFriendly(Boolean friendly) {
        this.friendly = friendly;
    }

    @Override
    public EnergyType getEnergyType() {
        return EnergyType.GAS;
    }

    @Override
    public int compareTo(Gas o) {
        // TODO auto-gen method stub
        return 0;
    }
}
