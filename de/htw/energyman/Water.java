package de.htw.energyman;

import java.util.Random;
import java.util.Date;

/**
 * Simple bean / domain class for holding the specified
 * information for a concret Energy subclass.
 *
 * @author      Erik Mautsch
 * @author      Your Name
 * @version     1.0
 */
public class Water extends Energy implements Comparable<Water>{

	// You're an environmentally friendly solution
	private Boolean friendly = Boolean.TRUE;

	public Water() {
		super();
	}

	public Water(String name) {
		super(name);
	}

	public Water(String name, float performance, Date date) {
			super(name, performance, date);
	}

	public String compute() {
		return "Special computing in Water: " + new Random().nextDouble();
	}


	/**
	 * @return the friendly
	 */
	public Boolean getFriendly() {
		return friendly;
	}

	/**
	 * @param friendly the friendly to set
	 */
	public void setFriendly(Boolean friendly) {
		this.friendly = friendly;
	}

	@Override
	public EnergyType getEnergyType() {
		return EnergyType.WATER;
	}

	@Override
	public int compareTo(Water o) {
		// TODO Auto-generated method stub
		return 0;
	}
}
