package de.htw.energyman.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

/**
 * Utility class for reading lines from csv files
 *
 * @author Erik Mautsch
 * @author Your Name
 * @version 1.0
 */
public class MagicCSV {

	private BufferedReader br = null;

	public MagicCSV(String filename) {
		try {
			br = new BufferedReader(new FileReader(new File(filename)));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String readRow() {
		try {
			return br.readLine();
		} catch (Exception e) {
			e.printStackTrace();
			return "ERROR";
		}
	}
}
