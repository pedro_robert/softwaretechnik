package de.htw.energyman.util;

import de.htw.energyman.EnergyManager;

import java.lang.Thread;

public class CSVThread extends Thread {
  private String filePath;
  public CSVThread(String filePath) {
    this.filePath = filePath;
  }
  public void run() {
    EnergyManager.loadCsv(filePath);
    EnergyManager.processCsv();
  }
}
