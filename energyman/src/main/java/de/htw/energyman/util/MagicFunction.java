package de.htw.energyman.util;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.math3.stat.descriptive.SummaryStatistics;

/**
 * An implementation for a very very <i>Magic Regression</i>.
 *
 *
 * @author Erik Mautsch
 *
 */

public class MagicFunction implements Function {

    private Map<Date, Double> foreCast;
    private Calendar cal;
    private Date date;

    public MagicFunction() {
      foreCast = new HashMap<Date, Double>();
      cal = Calendar.getInstance();
      cal.set(2015, Calendar.JULY, 1);
      date = cal.getTime();
    }

    public Double computeValue(double[] x, double[] y) {
      foreCast = compute(x, y);
      return foreCast.get(date);
    }

    public Map<Date, Double> compute(double[] x, double[] y) {
        // Get a SummaryStatistics instance
        SummaryStatistics stats = new SummaryStatistics();

        for(double value : y) {
            stats.addValue(value);
        }

        foreCast.put(date, stats.getMean());
        return foreCast;
    }
}
