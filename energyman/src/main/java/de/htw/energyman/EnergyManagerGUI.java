package de.htw.energyman;

import de.htw.energyman.gui.MainController;
import de.htw.energyman.energies.Energy;
import de.htw.energyman.energies.EnergyType;
import de.htw.energyman.gui.GUIThread;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class EnergyManagerGUI extends Application {

    public EnergyManager emanager = new EnergyManager();

    // window size needs to be calculated
    public static double emWidth = 800.00;
    public static double emHeight = 500.00;
    public static String windowTitle = "HTW - EnergyManager 2016";
    public static String fxmlLayout = "/fxml/energymanager.fxml";

    public static void main(String[] args) {
        // Application.launch(EnergyManagerGUI.class, args);
        GUIThread loadGUI = new GUIThread(args);
        loadGUI.start();
    }

    @Override
    public void start(Stage stage) throws Exception {
        FXMLLoader loader = new FXMLLoader();
        Parent root = (Parent) loader.load(getClass().getResourceAsStream(fxmlLayout));
        MainController mainController = loader.getController();
        mainController.setStage(stage);

        Scene mainScene = new Scene(root, emWidth, emHeight);

        stage.setTitle(windowTitle);
        stage.setScene(mainScene);

        /**/

        stage.show();
    }

    // private ObservableList<Energy> dumpData() {
    //     // return (ObservableList) emanager.getCustomers().get("Meier").getEnergyDataList(EnergyType.GAS);
    // }
}
