# EnergyMan

1. Build:

```
mvn clean install ([-q] to get rid of command line noise)
```

2. Run:

```
java -cp target/energyman-1.0-SNAPSHOT.jar de.htw.energyman.EnergyManager
```