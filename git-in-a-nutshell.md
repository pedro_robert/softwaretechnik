![git-logo](http://www.git-scm.com/images/logo@2x.png)

**Git** - A Version Control System
==================================
* Github (free; biggest git platform; all repositories are public)
* Bitbucket (free; also mercurial; private/public repositories)

Lets go throught our HelloWorldApp example!
-----------------------
```
$ mkdir hellow
$ cd hellow
$ git init
## start working
```

Adding your first File
----------------------
```
$ touch README.md
$ echo "# HelloWorld App!" >> README.md
```

Now what? Let's learn how git tells us what to do with `git status`
```
$ git status
On branch master

Initial commit

Untracked files:
  (use "git add <file>..." to include in what will be committed)

    README.md

nothing added to commit but untracked files present (use "git add" to track)
```

The last information is exactly what we want to do, tell git to track our README.md file

Let's follow the instruction, type `git add README.md` and check the new status

```
$ git add README.md
$ git status
On branch master

Initial commit

Changes to be committed:
  (use "git rm --cached <file>..." to unstage)

	new file:   README.md
```

Now we are able to create a new commit, a state where we can always go back on our project.

We do this by typing `git commit`, and all files on the staging area (the ones we added with `git add`) will be commited.

`git commit` will open your default editor and there you can add a commit message (mandatory), or we can write the message typing `-m "commit message"`

```
$ git commit -m "add README.md"
## now our git status shows that our working directory is clean
$ git status
On branch master
nothing to commit, working directory clean
```

Now let's add our HelloWorld.java and save our repository on Bitbucket

```
## let's check our code!
$ cat HelloWorld.java
public class HelloWorld {
	System.out.prinln("Hello World!")
}
## it looks all right, let's add and commit it
$ git add HelloWorld.java
$ git commit -m "add HelloWorld"
```

Now we want to share our code with our team, let's push it to Bitbucket

```
## tell git the online address and name it origin
$ git remote add origin https://bitbucket.org/user/repo.git
## tell git to push it to our master branch on our set origin
## --set-upstream so that next time we can just type `git push`
$ git push --set-upstream origin master
```

Now other users can get our project from bitbucket!

Let's use `git clone` to get the project on a new machine!

```
$ git clone https://bitbucket.org/user/hellow.git
$ cd hellow
$ git status
On branch master
Your branch is up-to-date with 'origin/master'.

nothing to commit, working directory clean
```

How can we check what has been done on the project? Let's use `git log` to see all commits that were made

```
$ git log
commit d7d50f2d195e01a4cbfb3084bcb1c0171d051c9e
Author: Robert G <robert.guenzler@student.htw-berlin.de>
Date:   Fri Jan 29 16:41:30 2016 +0100

    add HelloWorld

commit 61cb581b7e05c412c39f93a50ca5262225a4e7ca
Author: Robert G <robert.guenzler@student.htw-berlin.de>
Date:   Fri Jan 29 16:39:37 2016 +0100

    add README.md
```

It seems like the new user compiled the code and it threw an error! We need a main method for the HelloWorld class! The new user can modify it and push the changes to our Bitbucket repository!

```
## our new HelloWorld
$ cat HelloWorld.java
public class HelloWorld {
	public static void main(String[] args) {
		System.out.println("Hello World!");
	}
}
## let's see the status after we modified the file
$ git status
On branch master
Your branch is up-to-date with 'origin/master'.

Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working directory)

	modified:   HelloWorld.java

no changes added to commit (use "git add" and/or "git commit -a")
## let's add it to be commited
$ git add HelloWorld.java
$ git status
On branch master
Your branch is up-to-date with 'origin/master'.

Changes to be committed:
  (use "git reset HEAD <file>..." to unstage)

	modified:   HelloWorld.java
## let's commit it
$ git commit -m "add main method to HelloWorld"
## now let's push it (assuming the user has already set the upstream)
$ git push
## our working directory is clean again
```

Now we can the updated code that our friend wrote!

We will use `git fetch --all` to update all branches and `git rebase origin/master` to apply the changes to our working directory
```
$ git fetch --all
$ git rebase origin/master
## let's see what was done
$ git log
commit 5a7716856099a947e1cff6f25b264e10e9eca631
Author: Pedro Scaff <pedrinho.scaff@gmail.com>
Date:   Fri Jan 29 17:09:27 2016 +0100

    add main method to HelloWorld

commit d7d50f2d195e01a4cbfb3084bcb1c0171d051c9e
Author: Robert G <robert.guenzler@student.htw-berlin.de>
Date:   Fri Jan 29 16:41:30 2016 +0100

    add HelloWorld

commit 61cb581b7e05c412c39f93a50ca5262225a4e7ca
Author: Robert G <robert.guenzler@student.htw-berlin.de>
Date:   Fri Jan 29 16:39:37 2016 +0100

    add README.md
```

Yes! User Pedro corrected our code and now it works. With this flow we can work together on the project keeping track of what has been done!

More Online
-----------
* [Become a git guru](https://www.atlassian.com/git/tutorials/)
* [Udacity git course](https://www.udacity.com/courses/ud775)
* [Quick Reference](http://gitref.org)
* [Homepage](http://www.git-scm.com)
